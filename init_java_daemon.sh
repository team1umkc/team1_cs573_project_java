#!/bin/sh 
# Description: Bash Script to call into the LogWatcher Daemon.  Specify the directory to watch while starting it.
# Author: Jeff Lanning (Team 1)

# Setup variables
EXEC=/usr/bin/jsvc
JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64

# Directory where the application resides
DIR=/home/ubuntu/

# Any command line arguments to be passed to the our Java Daemon implementations init() method
ARGS="$@"

# Logging properties file
LOG_PROPERTIES_FILE=$DIR/src/log4j2.xml

CLASS_PATH="/home/ubuntu/commons-daemon-1.0.15.jar":"/home/ubuntu/cs573_project_log_watcher-1.0-jar-with-dependencies.jar"
CLASS=com.cs573.team1.project.filetracker.LogWatcherDaemon
USER=ubuntu
PID=/home/ubuntu/log_watcher_daemon.pid
LOG_OUT=/home/ubuntu/log_watcher_daemon.out
LOG_ERR=/home/ubuntu/log_watcher_daemon.err

do_exec()
{
    $EXEC -home "$JAVA_HOME" -Djava.util.logging.config.file=$LOG_PROPERTIES_FILE -cp $CLASS_PATH -user $USER -outfile $LOG_OUT -errfile $LOG_ERR -pidfile $PID $1 $CLASS $ARGS
}

case "$1" in
    start)
        echo "Starting File Watcher Daemon..."
        echo "$ARGS"
        do_exec
            ;;
    stop)
        echo "Stopping File Watcher Daemon..."
        do_exec "-stop"
            ;;
    restart)
        echo "Restarting File Watcher Daemon..."
        if [ -f "$PID" ]; then
            do_exec "-stop"
            do_exec
        else
            echo "Service not running, will do nothing"
            exit 1
        fi
            ;;
    *)
            echo "usage: daemon {start|stop|restart} optional arguments (for recursive watching): {-r} required argument: {path/to/dir}" >&2
            exit 3
            ;;
esac