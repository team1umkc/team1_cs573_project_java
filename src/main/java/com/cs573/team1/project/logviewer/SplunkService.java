package com.cs573.team1.project.logviewer;

import com.splunk.Application;
import com.splunk.Service;
import com.splunk.ServiceArgs;

/**
 * <li/>Description: Simple java app to call into our Splunk service.
 *
 * <li/>Author: Jeff Lanning (Team 1)
 */
public class SplunkService
{
    public static void main(String[] args)
    {
        ServiceArgs loginArgs = new ServiceArgs();
        loginArgs.setUsername("admin");
        loginArgs.setHost("http://ec2-54-69-209-32.us-west-2.compute.amazonaws.com:8000");
        loginArgs.setPort(8089);

        Service service = Service.connect(loginArgs);

        for (Application app : service.getApplications().values()) {
            System.out.println(app.getName());
        }
    }
}
