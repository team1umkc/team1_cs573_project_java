package com.cs573.team1.project.filetracker;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * <li/>Description: The Version Control Manager Creates/Initializes, Adds, Deletes, Commits, Displays Status/History.
 *
 * <li/>Author: Jeff Lanning (Team 1)
 */
public class VersionManager
{
    private static final Logger logger = LogManager.getLogger(VersionManager.class);
    public static boolean isExistingRepo = false;

    /* Creates/Initializes the version control repository

     */
    public static Repository OpenRepository(Path path) throws IOException, GitAPIException {
        Repository repository;

        try {
            if (!Files.exists(path)) {
                path = Files.createDirectory(path);
            }

            File repoFilePath = path.toFile();
            FileRepositoryBuilder builder = new FileRepositoryBuilder();
            repository = builder.setGitDir(repoFilePath)
                    .readEnvironment()
                    .findGitDir()
                    .build();


            logger.info("Repository is at: " + repository.getDirectory());

            // If the repository doesn't exist, create and initialize it.
            if (repository.isBare()) {
                repository = CreateAndInitRepo(repoFilePath);
            }
        } catch (IOException e) {
            logger.error("Exception occurred while attempting to access Git repository.\n", e);
            throw e;
        }

        return repository;
    }

    /*
        Initializes the repository. If it's already initialized, an exception is thrown indicating this.
     */
    public static Repository CreateAndInitRepo(final File filePath) throws IOException, GitAPIException {
        Repository repository = null;
        try {
            repository = FileRepositoryBuilder.create(new File(filePath.getAbsolutePath(), ".git"));
            repository.create();
            Git.init().setDirectory(filePath).call();
        } catch (IllegalStateException e) {
            // Eat exception since this indicates repo already exists.
            logger.info("Repository already exists at " + filePath +
                    ". Using existing repo.\n");
            isExistingRepo = true;
        } catch (GitAPIException e) {
            logger.error("Exception occurred while attempting to create/initialize the Git repository at: "
                    + filePath + ".\n", e);
            throw e;
        }

        return repository;
    }

    /*
        Adds a file/directory to the repository when one is added/modified.
     */
    public static boolean AddToRepository(final Git git, final Path filePath) throws GitAPIException {
        try {
            File file = filePath.toFile();
            File parentFile = filePath.getParent().toFile();
            String gitWorkPath = git.getRepository().getWorkTree().getAbsolutePath();
            String name;

            // If it's the root directory, just add file by name.  Otherwise, files added in sub-directories need their directory name to be added.
            if (file.getAbsolutePath().equalsIgnoreCase(gitWorkPath) || parentFile.getAbsolutePath().equalsIgnoreCase(gitWorkPath)) {
                name = file.getName();
            } else {
                while (parentFile != null && !parentFile.getAbsolutePath().equalsIgnoreCase(gitWorkPath)) {
                    file = parentFile;
                    parentFile = parentFile.getParentFile();
                }

                if (file.isDirectory() || parentFile == null) {
                    name = file.getName();
                } else {
                    name = parentFile.getName();
                }
            }

            git.add()
                .addFilepattern(name)
                .call();

            if (filePath.toFile().isDirectory()) {
                logger.info("Added directory successfully: " + filePath +
                        " ; to the repository.");
            } else {
                logger.info("Added file successfully: " + filePath +
                        " ; to the repository.");
            }

            return true;

        } catch (GitAPIException e) {
            logger.error("Exception occurred while attempting to add the file: "
                    + filePath + " to the Git repository.\n", e);
            throw e;
        }
    }

    /*
        Deletes a file from the repository.
     */
    public static boolean DeleteFromRepository(final Git git, final Path filePath) throws GitAPIException {
        try {
            File file = filePath.toFile();
            File parentFile = filePath.getParent().toFile();
            String gitWorkPath = git.getRepository().getWorkTree().getAbsolutePath();
            String name;

            // If it's the root directory, just add file by name.  Otherwise, files added in sub-directories need their directory name to be added.
            if (file.getAbsolutePath().equalsIgnoreCase(gitWorkPath) || parentFile.getAbsolutePath().equalsIgnoreCase(gitWorkPath)) {
                name = file.getName();
            } else {
                while (parentFile != null && !parentFile.getAbsolutePath().equalsIgnoreCase(gitWorkPath)) {
                    file = parentFile;
                    parentFile = parentFile.getParentFile();
                }

                if (file.isDirectory() || parentFile == null) {
                    name = file.getName();
                } else {
                    name = parentFile.getName();
                }
            }

            git.add()
               .setUpdate(true)
               .addFilepattern(name)
               .call();

            if (filePath.toFile().isDirectory()) {
                logger.info("Removed directory successfully: " + filePath +
                        " ; to the repository.");
            } else {
                logger.info("Removed file successfully: " + filePath +
                        " ; to the repository.");
            }

            return true;

        } catch (GitAPIException e) {
            logger.error("Exception occurred while attempting to remove the file: "
                    + filePath + " to the Git repository.\n", e);
            throw e;
        }
    }

    /*
        Commits changes in staging to version control.
     */
    public static boolean CommitToRepository(final Git git, final Path filePath) throws GitAPIException {
        try {
            Status status = git.status().call();
            if (!status.hasUncommittedChanges()) {
                return true;
            }

            git.commit()
                .setMessage(filePath.toString())
                .call();

            if (filePath.toFile().isDirectory()) {
                logger.info("Committed directory successfully: " + filePath +
                        " ; to the repository.");
            } else {
                logger.info("Committed file successfully: " + filePath +
                        " ; to the repository.");
            }

            RevCommit lastCommit = git.log().call().iterator().next();
            logger.info("Commit Log: " + lastCommit.getFullMessage() +
                    "  " + lastCommit.getCommitterIdent().toString()
                    + " SHA-ID: " + lastCommit.getId().getName() + "\n");

            return true;

        } catch (GitAPIException e) {
            logger.error("Exception occurred while attempting to commit file: "
                    + filePath + " to the Git repository.\n", e);
            throw e;
        }
    }

    /*
        Commits all changes in staging to version control.
     */
    public static boolean CommitAllToRepository(final Git git) throws GitAPIException {
        try {
            git.commit()
                .setMessage("Committing all Changes.")
                .setAll(true)
                .call();

            logger.info("Committed all was successful to the repository.\n");

            return true;

        } catch (GitAPIException e) {
            logger.error("Exception occurred while attempting to commit all to the Git repository.\n", e);
            throw e;
        }
    }

    /*
        Displays status of repository.
     */
    public static void DisplayStatus(final Git git) throws GitAPIException {
        try {
            Status status = git.status().call();
            logger.info("Added: " + status.getAdded());
            logger.info("Changed: " + status.getChanged());
            logger.info("Conflicting: " + status.getConflicting());
            logger.info("ConflictingStageState: " + status.getConflictingStageState());
            logger.info("IgnoredNotInIndex: " + status.getIgnoredNotInIndex());
            logger.info("Missing: " + status.getMissing());
            logger.info("Modified: " + status.getModified());
            logger.info("Removed: " + status.getRemoved());
            logger.info("Untracked: " + status.getUntracked());
            logger.info("UntrackedFolders: " + status.getUntrackedFolders());
        } catch (GitAPIException e) {
            logger.error("Exception occurred while attempting to show the status of the repository.\n", e);
            throw e;
        }
    }

    /*
        Displays the history of all commits made.
     */
    public static void DisplayCommitHistory(final Git git) throws IOException, GitAPIException {
        try {
            Iterable<RevCommit> commits = git.log().all().call();
            int totalCommits = 0;
            for (RevCommit commit : commits) {
                logger.info("Commit Log: " + commit.getFullMessage() +
                        "  " + commit.getCommitterIdent().toString()
                        + " SHA-ID: " + commit.getId().getName());
                totalCommits++;
            }

            logger.info("Total Commits: " + totalCommits + "\n\n");
        } catch (Exception e) {
            logger.error("Exception occurred while attempting to log the commit history.\n", e);
            throw e;
        }
    }
}