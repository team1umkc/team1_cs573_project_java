package com.cs573.team1.project.filetracker;

import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.apache.commons.daemon.DaemonInitException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

/**
 * <li/>Description: The daemon service runs in the background the {@link com.cs573.team1.project.filetracker.LogWatcher}.
 *                   This is invoked when the bash script is executed.
 *
 * <li/>Author: Jeff Lanning (Team 1)
 */
public class LogWatcherDaemon implements Daemon, Runnable {
    private static final Logger logger = LogManager.getLogger(LogWatcherDaemon.class);
    private Thread thread;
    private transient boolean working = false;
    LogWatcher dirWatcher;

    /*
     * Initializes the {@link LogWatcher}
     */
    @Override
    public void init(DaemonContext daemonContext) throws DaemonInitException, IOException {
        logger.info("\n\nInitializing Daemon....");
        String[] args = daemonContext.getArguments();
        logger.info("Number of arguments: " + args.length);

        boolean isRecursive = false;
        String path;
        if (args.length == 3) {
            logger.info("Specified Arguments: " + args[1] + " " + args[2]);
            String recursive = args[1];
            if (!recursive.isEmpty()) {
                if (recursive.equalsIgnoreCase("-r")) {
                    isRecursive = true;
                }
            }

            path = args[2];
        } else {
            logger.info("Specified Argument: " + args[1]);
            path = args[1];
        }

        if (path == null || path.isEmpty()) {
            path = "/home/ubuntu/file_repo";
        }

        Path pathToWatch = FileSystems.getDefault().getPath(path);
        dirWatcher = new LogWatcher(pathToWatch, isRecursive);
        thread = new Thread(dirWatcher);
    }

    @Override
    public void start() {
        logger.info("Starting Daemon....");
        working = true;
        thread.start();
    }

    @Override
    public void stop() throws Exception {
        if (dirWatcher != null) {
            dirWatcher.DisplayCommitHistoryAndStatus();
        }

        logger.info("Stopping Daemon....");
        working = false;
    }

    @Override
    public void destroy() {
        logger.info("Destroying Daemon....");
        thread = null;
    }

    @Override
    public void run() {
        while (working) {
            try {
                logger.info("Running Daemon....");
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // Intentionally eat exception...
            }
        }
    }
}
