package com.cs573.team1.project.filetracker;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

/**
 * <li/>Description: Creates a thread to locally test the {@link LogWatcher}
 *
 * <li/>Author: Jeff Lanning (Team 1)
 */
public class LogWatcherTest {
    public static void main(String[] args) throws InterruptedException, IOException
    {
        Path pathToWatch = FileSystems.getDefault().getPath("/home/ubuntu/file_repo");
        LogWatcher dirWatcher = new LogWatcher(pathToWatch, true);
        Thread dirWatcherThread = new Thread(dirWatcher);
        dirWatcherThread.start();

        // Interrupt the program after 5 minutes to stop it.
        Thread.sleep(300000);
        dirWatcherThread.interrupt();
    }
}
