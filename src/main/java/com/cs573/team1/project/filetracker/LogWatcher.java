package com.cs573.team1.project.filetracker;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Repository;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;

/**
 * <li/>Description: Handles watching directories/files using INotify (jnio) and
 *                   calling the {@link com.cs573.team1.project.filetracker.VersionManager} to add to version control.
 *
 * <li/>Author: Jeff Lanning (Team 1)
 */
public class LogWatcher implements Runnable {
    private static final Logger logger = LogManager.getLogger(LogWatcher.class);
    private Path path;
    private boolean recursive;
    WatchService watchService;
    Map<WatchKey, Path> keyMap;
    Git git = null;

    public LogWatcher(Path path, boolean recursive) throws IOException
    {
        this.path = path;
        this.recursive = recursive;
        this.watchService = FileSystems.getDefault().newWatchService();
        this.keyMap = new HashMap<>();
    }

    /*
     * Handles the watch events, checks for Create/Modify/Delete events and adds files to version control.
     */
    private void handleEvent(final Git git, final Path dir, final WatchEvent<?> event) throws GitAPIException, IOException {
        WatchEvent.Kind<?> kind = event.kind();
        if (kind.equals(StandardWatchEventKinds.OVERFLOW)) {
            logger.error("Event Overflow.");
            return;
        }

        Path watchPath = (Path) event.context();
        Path childPath = dir.resolve(watchPath);

        if (kind.equals(StandardWatchEventKinds.ENTRY_CREATE)) {
            logger.info("File/Path Created:" + childPath);

            if (VersionManager.AddToRepository(git, childPath)) {
                VersionManager.CommitToRepository(git, childPath);
            }

            if (recursive) {
                if (Files.isDirectory(childPath, LinkOption.NOFOLLOW_LINKS)) {
                    registerAllDirectories(childPath);
                }
            }
        } else if (kind.equals(StandardWatchEventKinds.ENTRY_DELETE)) {
            logger.info("File/Path Deleted:" + childPath);

            if (VersionManager.DeleteFromRepository(git, childPath)) {
                VersionManager.CommitToRepository(git, childPath);
            }

        } else if (kind.equals(StandardWatchEventKinds.ENTRY_MODIFY)) {
            logger.info("File/Path Modified:" + childPath);

            if (VersionManager.AddToRepository(git, childPath)) {
                VersionManager.CommitToRepository(git, childPath);
            }
        }
    }

   /*
    * Registers a directory to the {@link WatchService}
    */
    private void registerDirectory(final Path dir) throws IOException {
        if (dir == null) {
            logger.error("Given directory was NULL. Failed to register for watching.");
            return;
        }

        WatchKey watchKey = dir.register(watchService,
                StandardWatchEventKinds.ENTRY_CREATE,
                StandardWatchEventKinds.ENTRY_MODIFY,
                StandardWatchEventKinds.ENTRY_DELETE);

        Path existingDir = keyMap.get(watchKey);
        if (existingDir == null) {
            logger.info("Registering New Directory: " + dir.toAbsolutePath().toString() + "\n");
        } else {
            if (!dir.equals(existingDir)) {
                logger.info("Updating Directory from: " +
                        existingDir.toAbsolutePath().toString() + " to: " +
                        dir.toAbsolutePath().toString() + "\n");
            }
        }

        keyMap.put(watchKey, dir);
    }

    /*
     * Registers all directories and sub directories to the {@link WatchService}
     */
    private void registerAllDirectories(final Path dir) throws IOException {
        if (dir == null) {
            logger.error("Given directory was NULL. Failed to register for watching.");
            return;
        }

        Files.walkFileTree(dir, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                    throws IOException
            {
                // Don't watch the .git directory in the root watch folder.
                if (dir.toFile().getAbsolutePath().contains(".git")) {
                    return FileVisitResult.SKIP_SUBTREE;
                }

                registerDirectory(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    /*
     * Infinitely loops and handles watch events until no more watch events exist.
     */
    private void handleWatchEvents (final Git git) throws InterruptedException, GitAPIException, IOException {
        while (true) {
            WatchKey watchKey;
            watchKey = watchService.take();

            Path dir = keyMap.get(watchKey);
            if (dir == null) {
                logger.error("Invalid WatchKey!\n");
                continue;
            }

            for (final WatchEvent<?> event : watchKey.pollEvents()) {
                if (watchKey.isValid()) {
                    handleEvent(git, dir, event);
                }
            }

            if (!watchKey.reset()) {
                logger.info("WatchKey is no longer valid\n");
                watchKey.cancel();
                keyMap.remove(watchKey);

                if (keyMap.isEmpty()) {
                    break;
                }
            }
        }

        logger.info("Done handling watch events...");

        watchService.close();
    }

    /*
     * Registers the main watch directory. If recursive watching is enabled, all sub-directories/files are watched.
     */
    private void registerWatchDirectory(final Path dir) throws InterruptedException, GitAPIException, IOException {
        if (recursive) {
            registerAllDirectories(dir);
        } else {
            registerDirectory(dir);
        }
    }

    /*
     * If a new git repo is created in an existing folder structure, add all existing files to repo.
     */
    private void addExistingFilesToRepo(final Path dir, final Git git) throws IOException, GitAPIException {
        Files.walkFileTree(dir, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                    throws IOException
            {
                // Don't watch the .git directory in the root watch folder.
                if (dir.toFile().getAbsolutePath().contains(".git")) {
                    return FileVisitResult.SKIP_SUBTREE;
                }

                try {
                    // Add each DIRECTORY to git repo.
                    VersionManager.AddToRepository(git, dir);
                } catch (GitAPIException e) {
                    logger.error("Exception occurred while attempting to access the Git repository.\n", e);
                    Thread t = Thread.currentThread();
                    t.getUncaughtExceptionHandler().uncaughtException(t, e);
                }

                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(Path file,
                                             BasicFileAttributes attrs) throws IOException {
                try {
                    // Add each FILE to git repo.
                    VersionManager.AddToRepository(git, file);
                } catch (GitAPIException e) {
                    logger.error("Exception occurred while attempting to access the Git repository.\n", e);
                    Thread t = Thread.currentThread();
                    t.getUncaughtExceptionHandler().uncaughtException(t, e);
                }

                return FileVisitResult.CONTINUE;
            }
        });

        // Commit all files/directories to the repo.
        VersionManager.CommitAllToRepository(git);
    }

    /*
     * Display commit history and status of version control repo.
     */
    public void DisplayCommitHistoryAndStatus () {
        if (git == null) {
            logger.error("Git repository not initialized. Cannot display commit status.");
            return;
        }

        try {
            VersionManager.CommitAllToRepository(git);
            VersionManager.DisplayCommitHistory(git);
            VersionManager.DisplayStatus(git);
        } catch (GitAPIException e) {
            logger.error("Exception occurred while attempting to access the Git repository.\n", e);
            Thread t = Thread.currentThread();
            t.getUncaughtExceptionHandler().uncaughtException(t, e);
        } catch (IOException e ) {
            logger.error("Exception occurred while attempting to access the file system.\n", e);
            Thread t = Thread.currentThread();
            t.getUncaughtExceptionHandler().uncaughtException(t, e);
        }
    }

    /*
     * This gets called when the thread is created.  Main entry point.
     */
    @Override
    public void run() {
        try {
            Repository repository = VersionManager.OpenRepository(path);
            git = new Git(repository);

            logger.info(repository.getDirectory());

            // If it's not an existing repo, make sure to add any
            // existing files/directories to Git for Version Control.
            if (!VersionManager.isExistingRepo) {
                logger.info("Repository does NOT exist at: "
                        + repository.getDirectory() + " ; Adding all files in directory tree repo.");
                addExistingFilesToRepo(path, git);
            }

            // Register events to watch directory tree.
            registerWatchDirectory(path);
            handleWatchEvents(git);

        } catch (InterruptedException e) {
            logger.info("Thread Interrupted. Exiting...\n");
            DisplayCommitHistoryAndStatus();
        } catch (GitAPIException e) {
            logger.error("Exception occurred while attempting to access the Git repository.\n", e);
            Thread t = Thread.currentThread();
            t.getUncaughtExceptionHandler().uncaughtException(t, e);
        } catch (IOException e ) {
            logger.error("Exception occurred while attempting to access the file system.\n", e);
            Thread t = Thread.currentThread();
            t.getUncaughtExceptionHandler().uncaughtException(t, e);
        } catch (Exception e) {
            logger.error(e);
            Thread t = Thread.currentThread();
            t.getUncaughtExceptionHandler().uncaughtException(t, e);
        } finally {
            keyMap.clear();
            DisplayCommitHistoryAndStatus();
        }
    }
}