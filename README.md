# README #

This is the File Watcher and Modification Tracker (Version Manager) system.

## What is this repository for? ##

* The File Watcher and Modification Tracker system monitors the file system for creation, modifications, and deletions and automatically adds those changes to our Version Management system to track them and allow the user to recover previous versions.
* Version 1.0

## System Requirements ##
* Linux System
* Maven: http://maven.apache.org/download.cgi
* Git: http://git-scm.com/
* IntelliJ Idea: https://www.jetbrains.com/idea/download/
* Java JDK: 1.7 or higher.
* Apache Commons Daemon: http://commons.apache.org/proper/commons-daemon/download_daemon.cgi

## Developing the Code ##
Import the project in IntelliJ for development.  See the User Manual for more detailed instructions.

* For packaging the JAR, run the command: mvn clean compile assembly:single

## Who do I talk to? ##

* Contact Jeff Lanning from Team 1 for any assistance.